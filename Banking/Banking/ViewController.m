//
//  ViewController.m
//  Banking
//
//  Created by Azizi on 30/01/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init data containers
    bank = [[Bank alloc] init];
    contacts = [[Contacts alloc] init];
    transactions = [[Transactions alloc] init];
    [transactions addTransaction:[NSString stringWithFormat:@"%@ | Angel | %@ | %@", [bank currentTime], [bank getBalance], [bank getBalance]]];
    
    // Init view
    [self createMainView];
}

- (void)createMainView {
    // Init mainView sizes
    CGSize mainViewRect    = self.view.frame.size;
    CGPoint mainViewCenter = CGPointMake(mainViewRect.width / 2, mainViewRect.height / 2);
    [self.view setBackgroundColor:[UIColor darkGrayColor]];
    
    // Init label sizes
    CGSize labelViewRect    = CGSizeMake(150, 50);
    CGPoint labelViewCenter = CGPointMake(mainViewCenter.x - (labelViewRect.width / 2), 0);
    
    // Title label
    CGRect titleLabelRect = CGRectMake(labelViewCenter.x, 50, labelViewRect.width, labelViewRect.height);
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setText:@"Balance [EUR]"];
    [self.view addSubview:titleLabel];
    
    // Balance label
    CGRect balanceLabelRect = CGRectMake(labelViewCenter.x, titleLabelRect.origin.y + 50, labelViewRect.width, labelViewRect.height);
    balanceLabel = [[UILabel alloc] initWithFrame:balanceLabelRect];
    [balanceLabel setTextAlignment:NSTextAlignmentCenter];
    [balanceLabel setTextColor:[UIColor whiteColor]];
    [balanceLabel setText:[bank getBalance]];
    [self.view addSubview:balanceLabel];
    
    
    CGRect transferButtonRect = CGRectMake(0, balanceLabelRect.origin.y + 50, 150, 50);
    UIButton *transferButton = [[UIButton alloc] initWithFrame:transferButtonRect];
    
    [transferButton setBackgroundColor:[UIColor lightGrayColor]];
    [transferButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [transferButton setTitle:@"Transfer" forState:UIControlStateNormal];
    
    [transferButton addTarget:self action:@selector(createTransferView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:transferButton];
    
    
    CGRect transactionsButtonRect = CGRectMake(mainViewRect.width - 150, transferButtonRect.origin.y, 150, 50);
    UIButton *transactionsButton = [[UIButton alloc] initWithFrame:transactionsButtonRect];
    [transactionsButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [transactionsButton setBackgroundColor:[UIColor lightGrayColor]];
    [transactionsButton setTitle:@"Transactions" forState:UIControlStateNormal];
    
    [transactionsButton addTarget:self action:@selector(createTransactionsView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:transactionsButton];
}

- (void)updateBalance:(NSString *)amount {
    [balanceLabel setText:amount];
}


- (void)createTransferView {
    if (!transferView) {
        // Init mainView sizes
        CGSize mainViewRect    = self.view.frame.size;
        CGPoint mainViewCenter = CGPointMake(mainViewRect.width / 2, mainViewRect.height / 2);
        
        transferView = [[UIView alloc] initWithFrame:self.view.frame];
        [transferView setBackgroundColor:self.view.backgroundColor];
        
        // Back button
        CGRect backButtonRect = CGRectMake(0, 50, 150, 50);
        UIButton *backButton = [[UIButton alloc] initWithFrame:backButtonRect];
        
        [backButton setBackgroundColor:[UIColor lightGrayColor]];
        [backButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [backButton setTitle:@"Back" forState:UIControlStateNormal];
        
        [backButton addTarget:self action:@selector(hideTransferView) forControlEvents:UIControlEventTouchUpInside];
        [transferView addSubview:backButton];
        
        
        // Init label sizes
        CGSize labelViewSize    = CGSizeMake(150, 50);
        CGPoint labelViewCenter = CGPointMake(mainViewCenter.x - (labelViewSize.width / 2), 0);
        
        // Title label
        CGRect titleLabelRect = CGRectMake(labelViewCenter.x, 50, labelViewSize.width, labelViewSize.height);
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setText:@"Recipient"];
        [transferView addSubview:titleLabel];
        
        
        // Init contactsView sizes
        CGSize contactsViewSize = CGSizeMake(mainViewRect.width, mainViewRect.height / 6);
        CGRect contactsViewRect = CGRectMake(0, titleLabelRect.origin.y + 50, contactsViewSize.width, contactsViewSize.height);
        UIPickerView *contactsView = [[UIPickerView alloc] initWithFrame:contactsViewRect];
        [contactsView setDelegate:contacts];
        [transferView addSubview:contactsView];
        
        
        // Title label
        CGRect amountLabelRect = CGRectMake(10, contactsViewRect.origin.y + contactsViewSize.height + 10, 80, labelViewSize.height);
        UILabel *amountLabel = [[UILabel alloc] initWithFrame:amountLabelRect];
        [amountLabel setTextAlignment:NSTextAlignmentCenter];
        [amountLabel setTextColor:[UIColor whiteColor]];
        [amountLabel setText:@"Amount"];
        [transferView addSubview:amountLabel];
        
        
        // Amount text field
        CGRect amountFieldRect = CGRectMake(amountLabelRect.origin.x + amountLabelRect.size.width, amountLabelRect.origin.y + 5, mainViewRect.width - amountLabelRect.origin.x * 2 - amountLabelRect.size.width - 100, 40);
        amountField = [[UITextField alloc] initWithFrame:amountFieldRect];
        [amountField setBorderStyle:UITextBorderStyleRoundedRect];
        
        [amountField setKeyboardType:UIKeyboardTypeNumberPad];
        [amountField setPlaceholder:@"Amount"];
        
        [transferView addSubview:amountField];
        
        // Fractional text field
        CGRect fractionalFieldRect = CGRectMake(amountFieldRect.origin.x + amountFieldRect.size.width + 100, amountFieldRect.origin.y, amountLabelRect.origin.x * 2 - amountLabelRect.size.width - 30, 40);
        fractionalField = [[UITextField alloc] initWithFrame:fractionalFieldRect];
        [fractionalField setBorderStyle:UITextBorderStyleRoundedRect];
        
        [fractionalField setKeyboardType:UIKeyboardTypeNumberPad];
        [fractionalField setPlaceholder:@"Cents"];
        
        [transferView addSubview:fractionalField];
        
        
        // Send button
        CGRect sendButtonRect = CGRectMake(mainViewCenter.x - (150 / 2), amountFieldRect.origin.y + amountFieldRect.size.height + 40, 150, 50);
        UIButton *transferButton = [[UIButton alloc] initWithFrame:sendButtonRect];
        
        [transferButton setBackgroundColor:[UIColor lightGrayColor]];
        [transferButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [transferButton setTitle:@"Transfer" forState:UIControlStateNormal];
        
        [transferButton addTarget:self action:@selector(transferMoney) forControlEvents:UIControlEventTouchUpInside];
        [transferView addSubview:transferButton];
        
        
        // Add view
        [self.view addSubview:transferView];
    } else {
        [transferView setHidden:false];
    }
}

- (void)transferMoney {
    NSString *amountString     = amountField.text;
    NSString *fractionalString = fractionalField.text;
    
    if (amountString.length > 0 || fractionalString.length > 0) {
        if (amountString.length == 0) amountString = @"0";
        if (fractionalString.length == 0) fractionalString = @"0";
        
        NSString *details = [bank transferAmount:[amountField.text intValue] withFractional:[fractionalField.text intValue] toRecipient:[contacts selected]];
        if (details) {
            [transactions addTransaction:details];
            [self updateBalance:[bank getBalance]];
            
            [self hideTransferView];
            [self createTransactionsView];
        } else {
            [self errorAnimation];
        }
    } else {
        [self errorAnimation];
    }
}

- (void)errorAnimation {
    [amountField setBackgroundColor:[UIColor redColor]];
    [fractionalField setBackgroundColor:[UIColor redColor]];
    [NSTimer scheduledTimerWithTimeInterval:0.1 repeats:false block:^(NSTimer *timer) {
        [self->amountField setBackgroundColor:[UIColor whiteColor]];
        [self->fractionalField setBackgroundColor:[UIColor whiteColor]];
    }];
}


- (void)hideTransferView {
    [transferView endEditing:true];
    [transferView setHidden:true];
}


- (void)createTransactionsView {
    if (!transactionsView) {
        // Init mainView sizes
        CGSize mainViewRect    = self.view.frame.size;
        CGPoint mainViewCenter = CGPointMake(mainViewRect.width / 2, mainViewRect.height / 2);
        
        // Transactions view
        transactionsView = [[UIView alloc] initWithFrame:self.view.frame];
        [transactionsView setBackgroundColor:self.view.backgroundColor];
        
        // Back button
        CGRect backButtonRect = CGRectMake(0, 50, 150, 50);
        UIButton *backButton = [[UIButton alloc] initWithFrame:backButtonRect];
        
        [backButton setBackgroundColor:[UIColor lightGrayColor]];
        [backButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
        [backButton setTitle:@"Back" forState:UIControlStateNormal];
        
        [backButton addTarget:self action:@selector(hideTransactionsView) forControlEvents:UIControlEventTouchUpInside];
        [transactionsView addSubview:backButton];
        
        
        // Init label sizes
        CGSize labelViewSize    = CGSizeMake(150, 50);
        CGPoint labelViewCenter = CGPointMake(mainViewCenter.x - (labelViewSize.width / 2), 0);
        
        // Title label
        CGRect titleLabelRect = CGRectMake(labelViewCenter.x, 50, labelViewSize.width, labelViewSize.height);
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setText:@"Transactions"];
        [transactionsView addSubview:titleLabel];
        
        // Table view
        CGRect tableViewRect = CGRectMake(0, backButtonRect.origin.y + backButtonRect.size.height + 10, self.view.frame.size.width, self.view.frame.size.height - backButtonRect.size.height - 10);
        transactionsTableView = [[UITableView alloc] initWithFrame:tableViewRect style:UITableViewStylePlain];
        [transactionsTableView setBackgroundColor:self.view.backgroundColor];
        [transactionsTableView setSeparatorColor:[UIColor whiteColor]];
        
        [transactionsTableView setDelegate:transactions];
        [transactionsTableView setDataSource:transactions];
        
        [transactionsView addSubview:transactionsTableView];
        
        
        [self.view addSubview:transactionsView];
    } else {
        [transactionsTableView reloadData];
        [transactionsView setHidden:false];
    }
}

- (void)hideTransactionsView {
    [transactionsView setHidden:true];
}


@end
