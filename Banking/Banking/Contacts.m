//
//  balance.m
//  Banking
//
//  Created by Azizi on 07/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "Contacts.h"

@implementation Contacts

- (Contacts *)init {
    if (self = [super init]) {
        contacts = [[NSMutableArray alloc] initWithObjects:@"Angel", @"Alice", @"Bob", @"Charlie", @"Denese", @"Elise", nil];
        selected = [contacts objectAtIndex:0];
    }
    
    return self;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [contacts count];
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title = [contacts objectAtIndex:row];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    //Here, like the table view you can get the each section of each row if you've multiple sections
    selected = [contacts objectAtIndex:row];
    
    //Now, if you want to navigate then;
    // Say, OtherViewController is the controller, where you want to navigate:
    
    
}

- (NSString *)selected {
    return selected;
}


@end
