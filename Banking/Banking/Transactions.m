//
//  balance.m
//  Banking
//
//  Created by Azizi on 07/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "Transactions.h"

@implementation Transactions

- (Transactions *)init {
    if (self = [super init]) {
        transactions = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [transactions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"transactionsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *text = [transactions objectAtIndex:indexPath.row];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [cell.textLabel setText:text];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell setBackgroundColor:[UIColor darkGrayColor]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"-> %ld", (long)indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}


- (void)addTransaction:(NSString *)details {
    [transactions addObject:details];
}


@end
