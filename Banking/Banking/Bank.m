//
//  balance.m
//  Banking
//
//  Created by Azizi on 07/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "Bank.h"

@implementation Bank

- (Bank *)init {
    if (self = [super init]) {
        [self generateRandomBalance];
    }
    return self;
}

- (int)randomNumber { return [self randomNumberWithLow:0 andHigh:99]; }
- (int)randomNumberWithLow:(int)low andHigh:(int)high {
    return low + arc4random() % (high - low);
}

- (void)generateRandomBalance {
    int minBalance = 90, maxBalance = 110;
    balance = [self randomNumberWithLow:minBalance andHigh:maxBalance];
    if (balance != maxBalance) fractional = [self randomNumber];
}

// --------------------------------

- (NSString *)getBalance {
    return [NSString stringWithFormat:@"%d.%d", balance, fractional];
}

- (NSString *)transferAmount:(int)transferAmount withFractional:(int)transferFractional toRecipient:(NSString *)contactName {
    if (balance >= transferAmount) {
        if (balance == transferAmount && fractional < transferFractional) {
            return nil;
        } else {
            balance -= transferAmount;
            if (fractional < transferFractional) {
                balance -= 1;
                fractional = 100 - (transferFractional % fractional);
            } else {
                fractional -= transferFractional;
            }
            
            NSString *transferString = [NSString stringWithFormat:@"%d.%d", transferAmount, transferFractional];
            
            NSString *details = [NSString stringWithFormat:@"%@ | %@ | %@ | %@", [self currentTime], contactName, transferString, [self getBalance]];
            return details;
        }
    } else { return nil; }
}



- (NSString *)currentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    return [formatter stringFromDate:[NSDate date]];
}

@end
