//
//  balance.m
//  Banking
//
//  Created by Azizi on 07/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Transactions : NSObject <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray *transactions;
}

- (void)addTransaction:(NSString *)details;

@end
