//
//  balance.m
//  Banking
//
//  Created by Azizi on 07/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bank : NSObject {
    int balance, fractional;
}

- (NSString *)getBalance;
- (NSString *)transferAmount:(int)transferAmount withFractional:(int)transferFractional toRecipient:(NSString *)contactName;
- (NSString *)currentTime;

@end
