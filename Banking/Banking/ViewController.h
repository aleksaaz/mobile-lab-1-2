//
//  ViewController.h
//  Banking
//
//  Created by Azizi on 30/01/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bank.h"
#import "Contacts.h"
#import "Transactions.h"

@interface ViewController : UIViewController {
    UILabel *balanceLabel;
    
    int balance, balanceDecimals;
    
    Bank *bank;
    Contacts *contacts;
    Transactions *transactions;
    
    UIView *transferView;
    UITextField *amountField;
    UITextField *fractionalField;
    
    UIView *transactionsView;
    UITableView *transactionsTableView;
}


@end

