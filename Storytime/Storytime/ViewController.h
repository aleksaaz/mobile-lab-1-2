//
//  ViewController.h
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSS.h"
#import <SafariServices/SafariServices.h> // In-app browser

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate, UISearchBarDelegate, UIPickerViewDelegate> {
    RSS *rss;
    
    NSArray *entries;
    UITableView *entriesTableView;
    UIRefreshControl *refreshControl;
    NSTimer *refreshTimer;
    
    BOOL search;
    UISearchBar *searchBar;
    NSMutableArray *searchEntries;
    
    
    // Details
    UIViewController *detailViewController;
    UIView *detailsView;
    UILabel *titleLabel;
    UILabel *contentLabel;
    
    // Preferences
    UIViewController *preferencesViewController;
    UIView *preferencesView;
    
    UITextField *urlField;
    
    UIPickerView *limitPicker;
    NSArray *entryLimits;
    NSInteger selectedLimit;
    
    UIPickerView *refreshPicker;
    NSArray *refreshValues;
    NSInteger selectedRefresh;
    
    
    // Back button
    UIButton *backButton;
}


@end
