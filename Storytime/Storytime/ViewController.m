//
//  ViewController.m
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Init RSS
    rss = [[RSS alloc] init];
    
    // UI
    [self createMainView];
    [self initBackButton];
    
    // Init preferences
    entryLimits = [[NSArray alloc] initWithObjects:@"10", @"20", @"50", @"100", nil];
    refreshValues = [[NSArray alloc] initWithObjects:@"Every 10 min", @"Every half hour", @"Hourly", @"Daily", nil];
    
    if (![self preferencesForKey:@"url"]) {
        [self preferencesStoreValue:@"https://developer.apple.com/news/rss/news.rss" forKey:@"url"];
        [self preferencesStoreValue:@"3" forKey:@"limit"];
        [self preferencesStoreValue:@"0" forKey:@"refresh"];
    }
    
    [self initTimer];
}

- (void)viewDidAppear:(BOOL)animated {
    //[self refreshEntries];
    //[self createPreferencesView];
    NSLog(@"A wild view appeared!");
}

- (void)refreshEntries {
    // Refresh data
    [refreshControl beginRefreshing];
    [entriesTableView setContentOffset:CGPointMake(0, -refreshControl.frame.size.height -entriesTableView.tableHeaderView.frame.size.height) animated:true];
    
    [rss initWithURL:[self preferencesForKey:@"url"] completion:^(NSArray *data) {
        self->entries = data;
        [self->entriesTableView reloadData];
        [self->refreshControl endRefreshing];
    }];
}


- (void)createMainView {
    CGSize mainViewSize = self.view.frame.size;
    [self.view setBackgroundColor:[UIColor colorWithRed:25.0/255.0 green:25.0/255.0 blue:25.0/255.0 alpha:1.0]];
    
    // Table view
    CGRect tableViewRect = CGRectMake(0, 0, mainViewSize.width, mainViewSize.height);
    entriesTableView = [[UITableView alloc] initWithFrame:tableViewRect style:UITableViewStylePlain];
    [entriesTableView setBackgroundColor:self.view.backgroundColor];
    [entriesTableView setSeparatorColor:[UIColor whiteColor]];
    
    [entriesTableView setDelegate:self];
    [entriesTableView setDataSource:self];
    
    
    // Table view refresh control
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl setTintColor:[UIColor whiteColor]];
    [refreshControl addTarget:self action:@selector(refreshViewPulled:) forControlEvents:UIControlEventValueChanged];
    [entriesTableView setRefreshControl:refreshControl];
    
    
    // Table view header
    CGSize headerViewSize = CGSizeMake(mainViewSize.width, 44);
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, headerViewSize.width, headerViewSize.height)];
    
    // Search bar
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, headerViewSize.width - headerViewSize.height, 44)];
    [searchBar setBarTintColor:entriesTableView.backgroundColor];
    [searchBar setDelegate:self];
    [headerView addSubview:searchBar];
    
    // Preferences button
    UIButton *prefButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [prefButton setFrame:CGRectMake(headerViewSize.width - headerViewSize.height, 8, headerViewSize.height - 16, headerViewSize.height - 16)];
    [prefButton setImage:[UIImage imageNamed:@"prefs"] forState:UIControlStateNormal];
    [prefButton setTintColor:[UIColor whiteColor]];
    [prefButton addTarget:self action:@selector(createPreferencesView) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:prefButton];
    
    [entriesTableView setTableHeaderView:headerView];
    
    
    [self.view addSubview:entriesTableView];
}

- (void)refreshViewPulled:(UIRefreshControl *)refreshControl {
    [self refreshEntries];
}


// Safari View Controller
- (void)createSVCWithURL:(NSString *)url {
    SFSafariViewController *svc = [[SFSafariViewController alloc] initWithURL:[NSURL URLWithString:url]];
    [svc setDelegate:self];
    [self presentViewController:svc animated:true completion:nil];
}

// Init back button
- (void)initBackButton {
    CGSize mainViewSize    = self.view.frame.size;
    CGPoint mainViewCenter = CGPointMake(mainViewSize.width / 2, mainViewSize.height / 2);
    
    // Back button
    CGSize backButtonSize = CGSizeMake(150, 50);
    CGRect backButtonRect = CGRectMake(mainViewCenter.x - (backButtonSize.width / 2), mainViewSize.height - (mainViewSize.height / 20) - backButtonSize.height, backButtonSize.width, backButtonSize.height);
    backButton = [[UIButton alloc] initWithFrame:backButtonRect];
    
    [backButton setBackgroundColor:[UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0]];
    [backButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateHighlighted];
    [backButton setTitle:@"Dismiss" forState:UIControlStateNormal];
    
    [backButton addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
}

- (void)dismissViewController {
    [self dismissViewControllerAnimated:true completion:nil];
}



// Detail view (if there's no url)
- (void)createDetailView:(NSDictionary *)entry {
    if (!detailViewController) {
        detailViewController = [[UIViewController alloc] init];
        
        // Init mainView sizes
        CGSize mainViewSize    = self.view.frame.size;
        CGPoint mainViewCenter = CGPointMake(mainViewSize.width / 2, mainViewSize.height / 2);
        
        detailsView = [[UIView alloc] initWithFrame:self.view.frame];
        [detailsView setBackgroundColor:self.view.backgroundColor];
        [detailViewController setView:detailsView];
        
        
        // Back button
        [detailsView addSubview:backButton];
        
        
        // Init label sizes
        CGSize labelViewSize    = CGSizeMake(mainViewSize.width - 100, 150);
        CGPoint labelViewCenter = CGPointMake(mainViewCenter.x - (labelViewSize.width / 2), 0);
        
        // Title label
        CGRect titleLabelRect = CGRectMake(labelViewCenter.x, 100, labelViewSize.width, labelViewSize.height);
        titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
        
        [titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [titleLabel setNumberOfLines:3];
        
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor whiteColor]];
        
        [detailsView addSubview:titleLabel];
        
        // Content label
        CGRect contentLabelRect = CGRectMake(50, titleLabelRect.origin.y + titleLabelRect.size.height + 50, mainViewSize.width - 100, 400);
        contentLabel = [[UILabel alloc] initWithFrame:contentLabelRect];
        
        [contentLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [contentLabel setNumberOfLines:0];
        
        [contentLabel setTextColor:[UIColor whiteColor]];
        
        [detailsView addSubview:contentLabel];
    }
    
    NSString *title = [entry objectForKey:@"title"];
    NSString *content = [entry objectForKey:@"content"];
    
    [titleLabel setText:title];
    [contentLabel setText:content];
    
    [self presentViewController:detailViewController animated:true completion:nil];
}


// Preferences view
- (void)createPreferencesView {
    if (!preferencesViewController) {
        preferencesViewController = [[UIViewController alloc] init];
        
        // Init mainView sizes
        CGSize mainViewSize    = self.view.frame.size;
        CGPoint mainViewCenter = CGPointMake(mainViewSize.width / 2, mainViewSize.height / 2);
        
        preferencesView = [[UIView alloc] initWithFrame:self.view.frame];
        [preferencesView setBackgroundColor:self.view.backgroundColor];
        [preferencesViewController setView:preferencesView];
        
        
        // Apply button
        UIButton *applyButton = [[UIButton alloc] initWithFrame:CGRectMake(backButton.frame.origin.x, backButton.frame.origin.y - backButton.frame.size.height - 15, backButton.frame.size.width, backButton.frame.size.height)];
        
        [applyButton setBackgroundColor:backButton.backgroundColor];
        [applyButton setTitleColor:backButton.currentTitleColor forState:UIControlStateHighlighted];
        [applyButton setTitle:@"Apply" forState:UIControlStateNormal];
        
        [applyButton addTarget:self action:@selector(applyPreferences) forControlEvents:UIControlEventTouchUpInside];
        [preferencesView addSubview:applyButton];
        
        // Back button
        [preferencesView addSubview:backButton];
        
        
        // Init label sizes
        CGSize labelViewRect    = CGSizeMake(150, 50);
        CGPoint labelViewCenter = CGPointMake(mainViewCenter.x - (labelViewRect.width / 2), 0);
        
        // Title label
        CGRect titleLabelRect = CGRectMake(labelViewCenter.x, 50, labelViewRect.width, labelViewRect.height);
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleLabelRect];
        [titleLabel setTextAlignment:NSTextAlignmentCenter];
        [titleLabel setTextColor:[UIColor whiteColor]];
        [titleLabel setText:@"Preferences"];
        [preferencesView addSubview:titleLabel];
        
        
        // Amount text field
        CGRect urlFieldRect = CGRectMake(10, titleLabelRect.origin.y + titleLabelRect.size.height + 50, mainViewSize.width - 20, 40);
        urlField = [[UITextField alloc] initWithFrame:urlFieldRect];
        [urlField setBorderStyle:UITextBorderStyleRoundedRect];
        
        [urlField setKeyboardType:UIKeyboardTypeURL];
        [urlField setPlaceholder:@"URL"];
        
        [urlField setText:[self preferencesForKey:@"url"]];
        
        [preferencesView addSubview:urlField];
        
        
        // Limit label
        CGRect limitLabelRect = CGRectMake(urlFieldRect.origin.x, urlFieldRect.origin.y + urlFieldRect.size.height + 50, labelViewRect.width, labelViewRect.height);
        UILabel *limitLabel = [[UILabel alloc] initWithFrame:limitLabelRect];
        [limitLabel setTextAlignment:NSTextAlignmentLeft];
        [limitLabel setTextColor:[UIColor whiteColor]];
        [limitLabel setText:@"Display limit"];
        [preferencesView addSubview:limitLabel];
        
        // Limit Picker view
        limitPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, urlFieldRect.origin.y + urlFieldRect.size.height, mainViewSize.width, 200)];
        [limitPicker setDelegate:self];
        [preferencesView addSubview:limitPicker];

        // Refresh label
        CGRect refreshLabelRect = CGRectMake(urlFieldRect.origin.x, limitPicker.frame.origin.y + limitPicker.frame.size.height - 10, labelViewRect.width, labelViewRect.height);
        UILabel *refreshLabel = [[UILabel alloc] initWithFrame:refreshLabelRect];
        [refreshLabel setTextAlignment:NSTextAlignmentLeft];
        [refreshLabel setTextColor:[UIColor whiteColor]];
        [refreshLabel setText:@"Auto-refresh"];
        [preferencesView addSubview:refreshLabel];
        
        // Refresh Picker view
        refreshPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, refreshLabelRect.origin.y, mainViewSize.width, limitPicker.frame.size.height)];
        [refreshPicker setDelegate:self];
        [preferencesView addSubview:refreshPicker];
    }
    
    [urlField setText:[self preferencesForKey:@"url"]];
    [limitPicker selectRow:[[self preferencesForKey:@"limit"] integerValue] inComponent:0 animated:false];
    [refreshPicker selectRow:[[self preferencesForKey:@"refresh"] integerValue] inComponent:0 animated:false];
    
    [self presentViewController:preferencesViewController animated:true completion:nil];
}

- (void)preferencesStoreValue:(NSString *)content forKey:(NSString *)key {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    
    if (content.length == 0) {
        [preferences removeObjectForKey:key];
    } else {
        [preferences setObject:content forKey:key];
        [preferences synchronize];
    }
}

- (id)preferencesForKey:(NSString *)key {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    return [preferences objectForKey:key];
}

- (void)applyPreferences {
    [self preferencesStoreValue:urlField.text forKey:@"url"];
    [self preferencesStoreValue:[NSString stringWithFormat:@"%ld", (long)selectedLimit] forKey:@"limit"];
    [self preferencesStoreValue:[NSString stringWithFormat:@"%ld", (long)selectedRefresh] forKey:@"refresh"];
    [self initTimer]; // Re-init timer with new value
    [entriesTableView reloadData];
    [self dismissViewController];
}

- (void)initTimer {
    int currentInterval = [[self preferencesForKey:@"refresh"] intValue];
    float interval;
    switch (currentInterval) {
        case 0:  interval = 60.0f * 10.0f; // 10 min
        case 1:  interval = 60.0f * 30.0f; // 30 min
        case 2:  interval = 60.0f * 60.0f; // Hourly
        case 3:  interval = 60.0f * 60.0f * 12.0f; // Daily
        default: interval = 60.0f * 10.0f; // Default (10)
    }
    
    refreshTimer = [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(refreshEntries) userInfo:nil repeats:true];
}


// Table View

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (search) {
        return searchEntries.count;
    } else {
        return ([entries count] > 0) ? [[entryLimits objectAtIndex:[[self preferencesForKey:@"limit"] integerValue]] integerValue] : 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"transactionsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell setBackgroundColor:tableView.backgroundColor];
        NSString *text;
        
        if (search) {
            text = [searchEntries objectAtIndex:indexPath.row];
        } else {
            text = [[entries objectAtIndex:indexPath.row] objectForKey:@"title"];
        }
        
        [cell.textLabel setText:text];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *entry = [entries objectAtIndex:indexPath.row];
    NSString *url = [entry objectForKey:@"url"];
    
    if (url.length > 0) {
        // Display in-app browser
        [self createSVCWithURL:[entry objectForKey:@"url"]];
    } else {
        // Display detail view
        [self createDetailView:entry];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:true];
}


// Search bar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
        search = false;
        [searchBar endEditing:true];
    } else {
        search = true;
        searchEntries = [[NSMutableArray alloc] init];
        
        for (NSArray *entry in entries) {
            NSString *entryTitle = [entry valueForKey:@"title"];
            NSRange range = [entryTitle rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (range.location != NSNotFound) {
                [searchEntries addObject:entryTitle];
            }
        }
    }
    
    [entriesTableView reloadData];
}


// Picker view

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == limitPicker) {
        return [entryLimits count];
    } else {
        return [refreshValues count];
    }
}

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    if (pickerView == limitPicker) {
        title = [entryLimits objectAtIndex:row];
    } else {
        title = [refreshValues objectAtIndex:row];
    }
    
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == limitPicker) {
        selectedLimit = row;
    } else {
        selectedRefresh = row;
    }
}


@end
