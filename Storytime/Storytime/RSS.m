//
//  NSObject+RSS.m
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import "RSS.h"

@implementation RSS

- (RSS *)init {
    if (self = [super init]) {
        data = [[NSMutableArray alloc] init];
        loaded = false;
    }
    
    return self;
}


- (void)initWithURL:(NSURL *)url {
    MWFeedParser *feedParser = [[MWFeedParser alloc] initWithFeedURL:url];
    [feedParser setDelegate:self];
    [feedParser setFeedParseType:ParseTypeFull];
    [feedParser setConnectionType:ConnectionTypeSynchronously];
    [feedParser parse];
}

- (void)initWithURL:(NSString *)url completion:(void(^)(NSArray *))callback {
    [self initWithURL:[NSURL URLWithString:url]];
    [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:false block:^(NSTimer *timer) {
        if (self->loaded) {
            callback(self->data);
        }
    }];
}


// Add entries
- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item { // Provides info about a feed item
    NSDictionary *entry = [[NSDictionary alloc] initWithObjects:@[item.title, item.link, item.summary] forKeys:@[@"title", @"url", @"content"]];
    [data addObject:entry];
    /*
     item.title (NSString)
     item.link (NSString)
     item.author (NSString)
     item.date (NSDate)
     item.updated (NSDate)
     item.summary (NSString)
     item.content (NSString)
     item.enclosures (NSArray of NSDictionary with keys url, type and length)
     item.identifier (NSString)
     */
}


- (void)feedParserDidFinish:(MWFeedParser *)parser { // Parsing complete or stopped at any time by `stopParsing`
    loaded = true;
}

- (void)feedParser:(MWFeedParser *)parser didFailWithError:(NSError *)error { // Parsing failed
    NSLog(@"feedParser didFailWithError");
}

- (NSArray *)entries {
    return data;
}

@end
