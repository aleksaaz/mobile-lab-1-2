//
//  RSS.h
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MWFeedParser.h"

@interface RSS : NSObject <MWFeedParserDelegate> {
    NSMutableArray *data;
    BOOL loaded;
}

- (void)initWithURL:(NSString *)url completion:(void(^)(NSArray *))callback;



@end
