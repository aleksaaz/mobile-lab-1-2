//
//  main.m
//  Storytime
//
//  Created by Azizi on 20/03/2019.
//  Copyright © 2019 Aleksander Azizi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
